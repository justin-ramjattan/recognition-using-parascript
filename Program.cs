﻿/**
 * Import Parascript.FormXtra.Core.dll
 * Import Parascript.FormXtra.Recognation.dll
 * Import Parascript.FormXtra.RecognationCore.dll
 */

using System;
using System.IO;
using System.Reflection;
using Parascript.FormXtra.Recognition;

class Program
{
    static void Main()
    {

        AppDomain.CurrentDomain.AssemblyResolve += new ResolveEventHandler(ResolveAssemblyHandler);


        try
        {
            RecognizeImage();
        }
        catch (Exception e)
        {
            Console.WriteLine("{0}", e.ToString());
        }
        Console.ReadLine();
    }

    private static void RecognizeImage()
    {
        RecognitionParameters parameters = new RecognitionParameters()
        {
            FieldType = FieldTypes.Alphanumeric,
            WritingStyle = WritingStyles.Cursive        
        };
        using (FieldReader fieldReader = new FieldReader(parameters))
        {
            foreach (FileInfo fileInfo in new DirectoryInfo(@"C:\Users\JustinRamjattan\Desktop").GetFiles("textimage.png")) //Change this to the image file location and file name  
            {
                RecognitionAnswer answer = fieldReader.Recognize(fileInfo.FullName);

                Console.WriteLine(answer.Text);
            }
        }

    }
    private static Assembly ResolveAssemblyHandler(object sender, ResolveEventArgs args)
    {
        string[] parts = args.Name.Split(',');

        if (parts[0].EndsWith(".resources", StringComparison.OrdinalIgnoreCase))
        {
            return null;
        }

        string pathInFxFolder = Path.Combine(@"C:\Program Files\Parascript\FormXtra SDK 7", parts[0] + ".dll"); //Change this to the folder you imported the dll files if different.
        if (File.Exists(pathInFxFolder))
        {
            return Assembly.LoadFrom(pathInFxFolder);
        }
        else
        {
            return null;
        }
    }
}
